from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from app.forms import SignUpForm
# Create your views here.
class SignUp(generic.CreateView):
    form_class=SignUpForm
    success_url=reverse_lazy('login')
    template_name='signup.html'